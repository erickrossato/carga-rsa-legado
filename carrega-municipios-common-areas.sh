#!/bin/sh

DB_NAME=common_areas
DB_USER=geoserver
DB_SCHEMA=$DB_USER
DB_HOST=common-areas.ct9lbpnxzlpa.sa-east-1.rds.amazonaws.com
SQL_OUTPUT_FOLDER=sqls
OUTPUT_FOLDER=out
BACKUP_FOLDER=bkp
SHP_BASE_FOLDER=shp
TODAY=$( date +%Y-%m-%d )
export PGPASSWORD=bgzCK53A1a6VKupR1WsdUjgH6fORuQwb

SQL_OUTPUT_FOLDER=${SQL_OUTPUT_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}
OUTPUT_FOLDER=${OUTPUT_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}
BACKUP_FOLDER=${BACKUP_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}

if [ ! -d "$SQL_OUTPUT_FOLDER" ]; then
	mkdir -p $SQL_OUTPUT_FOLDER
fi

if [ ! -d "$OUTPUT_FOLDER" ]; then
	mkdir -p $OUTPUT_FOLDER
fi

if [ ! -d "$BACKUP_FOLDER" ]; then
	mkdir -p $BACKUP_FOLDER
fi

echo "Backing up table municipio"
pg_dump --host $DB_HOST --username $DB_USER -d $DB_NAME -t "municipio" > ${BACKUP_FOLDER}/municipio.sql

echo "Loading file to specified database ${DB_NAME}@${DB_HOST} with user $DB_USER";

for shapeFile in $( ls $SHP_BASE_FOLDER/*/*.shp  ); do

	folder=$( dirname $shapeFile )
	baseName=$( basename $shapeFile )
	
	echo "Generating SQL file for shapeFile $shapeFile"
	shp2pgsql -s 4674 -D $shapeFile $folder > ${SQL_OUTPUT_FOLDER}/${baseName}.sql

	echo "Loading to database"
	psql -d $DB_NAME -f ${SQL_OUTPUT_FOLDER}/${baseName}.sql -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/${baseName}.sql.out 2>&1

done

if [ -f "$1" ]; then

	echo "Running script $1 on database"
	psql -d $DB_NAME -f $1 -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/$1.out 2>&1

	echo "VACUUM ANALYZING municipio table"
	psql -d $DB_NAME -h $DB_HOST -U $DB_USER -c "VACUUM ANALYZE municipio;" 2>&1

fi
