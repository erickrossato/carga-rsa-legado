delete from gleba_risco gr where gr.idt_area_risco in (select idt_area_risco from area_risco where idt_base_risco IN (6));
delete from area_risco where idt_base_risco IN (6);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 6, 'Áreas Quilombolas - INCRA', ST_Force2D(geom) from "shp/quilombolas" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 6;

analyze area_risco;

insert into gleba_risco(idt_gleba, idt_area_risco, num_distancia)
select
	g.idt_gleba, ar.idt_area_risco, ST_Distance(g.geo_gleba, ar.geo_area_risco)
from 
	gleba g
	join area_risco ar on ST_DWithin(g.geo_gleba, ar.geo_area_risco, 0.005)
where
	ST_Distance(g.geo_gleba, ar.geo_area_risco) < 0.005
	AND ar.idt_base_risco IN (6);

drop table "shp/quilombolas";
