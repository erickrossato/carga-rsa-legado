delete from area_risco where idt_base_risco IN (2);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 2, 'Un. de Conservação', ST_Force2D(geom) from "shp/uc_restritiva" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 2;

analyze area_risco;

drop table "shp/uc_restritiva";
