delete from area_risco where idt_base_risco IN (1);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 1, 'Terras Indígenas', ST_Force2D(geom) from "shp/terras_indigenas" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 1;

analyze area_risco;

drop table "shp/terras_indigenas";
