
#!/bin/bash
OPERACAO=$1
ORIGEM=

FILE_ORIGEM_EMBARGOS_IBAMA="origem/embargos_ibama"
FILE_ORIGEM_QUILOMBOLAS="origem/quilombolas"
FILE_ORIGEM_INDIGENAS="origem/terras_indigenas"
FILE_ORIGEM_ICMBIO="origem/embargos_icmbio"
FILE_ORIGEM_RESTRITIVA="origem/uc_restritiva"
FILE_ORIGEM_N_RESTRITIVA="origem/uc_nao_restritiva" 

FILE_SHP_EMBARGOS_IBAMA="shp/embargos_ibama"
FILE_SHP_QUILOMBOLAS="shp/quilombolas"
FILE_SHP_INDIGENAS="shp/terras_indigenas"
FILE_SHP_ICMBIO="shp/embargos_icmbio"
FILE_SHP_RESTRITIVA="shp/uc_restritiva"
FILE_SHP_N_RESTRITIVA="shp/uc_nao_restritiva" 

#Parametros para criacao de diretorio de BKP
DIA=$(date | cut -d" " -f3)
MES=$(date | cut -d" " -f2)
ANO=$(date | cut -d" " -f6)
HORAMINUTO=$(date | cut -d" " -f4)
ARQUIVOBKP=./origem/$DIA$MES$ANO"_"$HORAMINUTO

#echo "Copiando os arquivos para o diretório de execução"
#Copiar arquivos da area Ponte e processar por base
# 1- Embargos Ibama
# 2- Quilombolas
# 3- Terras indigenas
# 4- Emb ICMBIO
# 5- UCS Restritivas
# 6- UCS Nao Restritivas

#LISTA=`ls -d /home/erickdreger/Downloads/OneDrive_2019-02-05/GCA-15004fev19/*`

#echo "Renomeando os arquivos para permitir o processamento"
#Disponibilizado os arquivos na area ponte copiar eles para o diretório local ./shp e renomear os diretórios conforme padrão apresentado
# 1- embargos_ibama
# 2- quilombolas
# 3- terras_indigenas
# 4- embargos_icmbio
# 5- uc_restritiva
# 6- uc_nao_restritiva

#Executar o Script por base 
echo "Inicio do processamento de carga das bases do RSA"
cd /srv/scripts/carga_legado/ 
mkdir $ARQUIVOBKP 
 
      aws s3 sync s3://rsa-prod-carga-legado /srv/scripts/carga_legado/origem 
      #Carga Embargos Ibama
      if [ -d "$FILE_ORIGEM_EMBARGOS_IBAMA" ] ; then
          echo "Tem arquivo Embargos Ibama: Processando..."
          echo "Processando Commom Areas"
          rm -fr $FILE_SHP_EMBARGOS_IBAMA
          cp -rf $FILE_ORIGEM_EMBARGOS_IBAMA ./shp 
          ./carrega-shape-common-areas.sh load_ibama_common_areas.sql
           
          echo "Audsat"
          rm -fr $FILE_SHP_EMBARGOS_IBAMA
          cp -rf $FILE_ORIGEM_EMBARGOS_IBAMA ./shp
          ./carrega-shape.sh load_ibama_audsat.sql servers_audsat_prod.txt embargos_ibama

          echo "Sicredi"
          rm -fr $FILE_SHP_EMBARGOS_IBAMA
          cp -rf $FILE_ORIGEM_EMBARGOS_IBAMA ./shp 
          ./carrega-shape.sh load_ibama_sicredi.sql servers_sicredi_prod.txt embargos_ibama
          rm -fr $FILE_SHP_EMBARGOS_IBAMA
          mv $FILE_ORIGEM_EMBARGOS_IBAMA $ARQUIVOBKP
      fi
  
        #Carga Quilombolas
        if [ -d "$FILE_ORIGEM_QUILOMBOLAS" ] ; then
            echo "Tem arquivo Quilombolas: Processando..."
            
            echo "Audsat"
            rm -fr $FILE_SHP_QUILOMBOLAS
            cp -rf $FILE_ORIGEM_QUILOMBOLAS ./shp 
            ./carrega-shape.sh load_quilombolas_audsat.sql servers_audsat_prod.txt quilombolas

            echo "Sicredi"
            rm -fr $FILE_SHP_QUILOMBOLAS
            cp -rf $FILE_ORIGEM_QUILOMBOLAS ./shp 
            ./carrega-shape.sh load_quilombolas_sicredi.sql servers_sicredi_prod.txt quilombolas

            echo "Commom Areas"
            rm -fr $FILE_SHP_QUILOMBOLAS
            cp -rf $FILE_ORIGEM_QUILOMBOLAS ./shp 
            ./carrega-shape-common-areas.sh load_quilombolas_common_areas.sql
            rm -fr $FILE_SHP_QUILOMBOLAS
            mv $FILE_ORIGEM_QUILOMBOLAS $ARQUIVOBKP

        fi


        if [ -d "$FILE_ORIGEM_INDIGENAS" ] ; then
            echo "Tem arquivo Indigenas: Processando..."

            echo "Audsat"
            rm -fr $FILE_SHP_INDIGENAS
            cp -rf $FILE_ORIGEM_INDIGENAS ./shp 
            ./carrega-shape.sh load_indigenas_audsat.sql servers_audsat_prod.txt terras_indigenas

            echo "Sicredi"
            rm -fr $FILE_SHP_INDIGENAS
            cp -rf $FILE_ORIGEM_INDIGENAS ./shp 
            ./carrega-shape.sh load_indigenas_sicredi.sql servers_sicredi_prod.txt terras_indigenas

            echo "Commom Areas"
            rm -fr $FILE_SHP_INDIGENAS
            cp -rf $FILE_ORIGEM_INDIGENAS ./shp 
            ./carrega-shape-common-areas.sh load_indigenas_common_areas.sql
            rm -fr $FILE_SHP_INDIGENAS
            mv $FILE_ORIGEM_INDIGENAS $ARQUIVOBKP
        fi


        if [ -d "$FILE_ORIGEM_ICMBIO" ] ; then
            echo "Tem arquivo ICMBIO: Processando..."

            echo "Audsat"
            rm -fr $FILE_SHP_ICMBIO
            cp -rf $FILE_ORIGEM_ICMBIO ./shp 
            ./carrega-shape.sh load_icmbio_audsat.sql servers_audsat_prod.txt embargos_icmbio

            echo "Sicredi"
            rm -fr $FILE_SHP_ICMBIO
            cp -rf $FILE_ORIGEM_ICMBIO ./shp 
            ./carrega-shape.sh load_icmbio_sicredi.sql servers_sicredi_prod.txt embargos_icmbio
            echo "Commom Areas"
            rm -fr $FILE_SHP_ICMBIO
            cp -rf $FILE_ORIGEM_ICMBIO ./shp 
            ./carrega-shape-common-areas.sh load_icmbio_common_areas.sql 
            rm -fr $FILE_SHP_ICMBIO
            mv $FILE_ORIGEM_ICMBIO $ARQUIVOBKP
        fi


        if [ -d "$FILE_ORIGEM_RESTRITIVA" ] ; then
            echo "Tem arquivo UC Restritiva: Processando..."

            echo "Audsat"
            rm -fr $FILE_SHP_RESTRITIVA
            cp -rf $FILE_ORIGEM_RESTRITIVA ./shp 
            ./carrega-shape.sh load_uc_r_audsat.sql servers_audsat_prod.txt uc_restritiva

            echo "Sicredi"
            rm -fr $FILE_SHP_RESTRITIVA
            cp -rf $FILE_ORIGEM_RESTRITIVA ./shp 
            ./carrega-shape.sh load_uc_r_sicredi.sql servers_sicredi_prod.txt uc_restritiva

            echo "Commom Areas"
            rm -fr $FILE_SHP_RESTRITIVA
            cp -rf $FILE_ORIGEM_RESTRITIVA ./shp 
            ./carrega-shape-common-areas.sh load_uc_r_common_areas.sql
            rm -fr $FILE_SHP_RESTRITIVA
            mv $FILE_ORIGEM_RESTRITIVA $ARQUIVOBKP
        fi

        if [ -d "$FILE_ORIGEM_N_RESTRITIVA" ] ; then
            echo "Tem arquivo UC NAO RESTRITIVA: Processando..."

            echo "Audsat"
           rm -fr $FILE_SHP_N_RESTRITIVA
             cp -rf $FILE_ORIGEM_N_RESTRITIVA ./shp 
           ./carrega-shape.sh load_uc_nr_audsat.sql servers_audsat_prod.txt uc_nao_restritiva
            echo "Sicredi"
            rm -fr $FILE_SHP_N_RESTRITIVA
            cp -rf $FILE_ORIGEM_N_RESTRITIVA ./shp 
            ./carrega-shape.sh load_uc_nr_sicredi.sql servers_sicredi_prod.txt uc_nao_restritiva

            echo "Commom Areas"
            rm -fr $FILE_SHP_N_RESTRITIVA
            cp -rf $FILE_ORIGEM_N_RESTRITIVA ./shp 
            ./carrega-shape-common-areas.sh load_uc_nr_common_areas.sql
            rm -fr $FILE_SHP_N_RESTRITIVA
            mv $FILE_SHP_N_RESTRITIVA $ARQUIVOBKP
        fi

aws s3 rm s3://rsa-prod-carga-legado --recursive
echo "Fim do processamento"
