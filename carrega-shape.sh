#!/bin/bash

DB_USER=usr_geo_credito_agricola
DB_SCHEMA=$DB_USER
SQL_OUTPUT_FOLDER=sqls
OUTPUT_FOLDER=out
BACKUP_FOLDER=bkp
SHP_BASE_FOLDER=shp/$3
TODAY=$( date +%Y-%m-%d_%HH%MM )
BKP=$4

export PGPASSWORD=4cd37d4fa6d54af29c113b5625434981

correctDbfEncoding() {

	baseFolder=$( dirname $1 )
	shpName=$( basename $1 .shp )
	cpgFile=$baseFolder/$shpName.cpg
	dbfFile=$baseFolder/$shpName.dbf
	csvFile=$baseFolder/$shpName.csv
	clearCsvFile=$baseFolder/${shpName}_clear.csv
	if [ ! -f $cpgFile ]; then
		echo "UTF-8" > $cpgFile
	fi
	ogr2ogr -f "CSV" $csvFile $dbfFile 2>/dev/null
	
	rm $dbfFile
	iconv -c -f "UTF-8" -t "UTF-8" $csvFile > $clearCsvFile
	ogr2ogr -f "ESRI Shapefile" $dbfFile $clearCsvFile 2>/dev/null
	
	rm $csvFile
	rm $clearCsvFile

}

while read line
do
	lineSplit=($line)
	dbName=${lineSplit[0]}
	dbHost=${lineSplit[1]}
	sqlOutputFolder=${SQL_OUTPUT_FOLDER}/${dbHost}/${dbName}/${TODAY}
	outputFolder=${OUTPUT_FOLDER}/${dbHost}/${dbName}/${TODAY}
	backupFolder=${BACKUP_FOLDER}/${dbHost}/${dbName}/${TODAY}

	if [ ! -d "$sqlOutputFolder" ]; then
		mkdir -p $sqlOutputFolder
	fi

	if [ ! -d "$outputFolder" ]; then
		mkdir -p $outputFolder
	fi

	if [ ! -d "$backupFolder" ]; then
		mkdir -p $backupFolder
	fi

	echo "Starting to process data for $dbName@$dbHost"
	echo "Backing up tables gleba_risco and area_risco"
	#pg_dump --host $dbHost --username $DB_USER -d $dbName -t 'area_risco' > ${backupFolder}/area_risco.sql
	#pg_dump --host $dbHost --username $DB_USER -d $dbName -t 'gleba_risco' > ${backupFolder}/gleba_risco.sql
	echo "Starting to load shape files";

	for shapeFile in $( ls $SHP_BASE_FOLDER/*.shp  ); do

		folder=$( dirname $shapeFile )
		baseName=$( basename $shapeFile )

		echo "Cleaning up invalid characters from shapeFile $shapeFile"
		correctDbfEncoding $shapeFile 2>&1 1>/dev/null

		echo "Generating SQL file for shapeFile $shapeFile"
		shp2pgsql -s 4674 -W 'LATIN1' -D $shapeFile $folder > ${sqlOutputFolder}/${baseName}.sql 2> /dev/null

		echo "Loading to database"
		psql -d $dbName -f ${sqlOutputFolder}/${baseName}.sql -h $dbHost -U $DB_USER -o ${outputFolder}/${baseName}.sql.out 2>&1
		
		break

	done

	if [ -f "$1" ]; then
		
		echo "VACUUM ANALYZING gleba table"
	#	psql -d $dbName -h $dbHost -U $DB_USER -c 'VACUUM ANALYZE gleba;' 2>&1 1>/dev/null

		echo "Running script $1 on database"
		psql -d $dbName -f $1 -h $dbHost -U $DB_USER -o ${outputFolder}/$1.out

		echo "VACUUM area_risco table"
	#	psql -d $dbName -h $dbHost -U $DB_USER -c 'VACUUM area_risco;' 2>&1 1>/dev/null

		echo "VACUUM ANALYZING gleba_risco table"
	#	psql -d $dbName -h $dbHost -U $DB_USER -c 'VACUUM ANALYZE gleba_risco;' 2>&1 1>/dev/null

	fi
done < $2
