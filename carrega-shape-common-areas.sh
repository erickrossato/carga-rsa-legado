#!/bin/sh

DB_NAME=common_areas
DB_USER=geoserver
DB_SCHEMA=$DB_USER
DB_HOST=common-areas.ct9lbpnxzlpa.sa-east-1.rds.amazonaws.com
SQL_OUTPUT_FOLDER=sqls
OUTPUT_FOLDER=out
BACKUP_FOLDER=bkp
SHP_BASE_FOLDER=shp
TODAY=$( date +%Y-%m-%d )
export PGPASSWORD=bgzCK53A1a6VKupR1WsdUjgH6fORuQwb

correctDbfEncoding() {

	baseFolder=$( dirname $1 )
	shpName=$( basename $1 .shp )
	cpgFile=$baseFolder/$shpName.cpg
	dbfFile=$baseFolder/$shpName.dbf
	csvFile=$baseFolder/$shpName.csv
	clearCsvFile=$baseFolder/${shpName}_clear.csv
	if [ ! -f $cpgFile ]; then
		echo "UTF-8" > $cpgFile
	fi
	ogr2ogr -f "CSV" $csvFile $dbfFile 2>/dev/null
	
	rm $dbfFile
	iconv -c -f "UTF-8" -t "UTF-8" $csvFile > $clearCsvFile
	ogr2ogr -f "ESRI Shapefile" $dbfFile $clearCsvFile 2>/dev/null
	
	rm $csvFile
	rm $clearCsvFile

}

SQL_OUTPUT_FOLDER=${SQL_OUTPUT_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}
OUTPUT_FOLDER=${OUTPUT_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}
BACKUP_FOLDER=${BACKUP_FOLDER}/${DB_HOST}/${DB_NAME}/${TODAY}

if [ ! -d "$SQL_OUTPUT_FOLDER" ]; then
	mkdir -p $SQL_OUTPUT_FOLDER
fi

if [ ! -d "$OUTPUT_FOLDER" ]; then
	mkdir -p $OUTPUT_FOLDER
fi

if [ ! -d "$BACKUP_FOLDER" ]; then
	mkdir -p $BACKUP_FOLDER
fi

echo "Backing up tables area_risco"
#pg_dump --host $DB_HOST --username $DB_USER -d $DB_NAME -t "area_risco" > ${BACKUP_FOLDER}/area_risco.sql

echo "Loading file to specified database ${DB_NAME}@${DB_HOST} with user $DB_USER";

for shapeFile in $( ls $SHP_BASE_FOLDER/*/*.shp  ); do

	folder=$( dirname $shapeFile )
	baseName=$( basename $shapeFile )

	echo "Cleaning up invalid characters from shapeFile $shapeFile"
	correctDbfEncoding $shapeFile 2>&1 1>/dev/null
	
	echo "Generating SQL file for shapeFile $shapeFile"
	shp2pgsql -s 4674 -W "LATIN1" -D $shapeFile $folder > ${SQL_OUTPUT_FOLDER}/${baseName}.sql

	echo "Loading to database"
	psql -d $DB_NAME -f ${SQL_OUTPUT_FOLDER}/${baseName}.sql -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/${baseName}.sql.out 2>&1

done

if [ -f "$1" ]; then

	echo "Running script $1 on database"
	psql -d $DB_NAME -f $1 -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/$1.out

	echo "VACUUMING area_risco table"
	psql -d $DB_NAME -h $DB_HOST -U $DB_USER -c "VACUUM area_risco;" 2>&1

fi
