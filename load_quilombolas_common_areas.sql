delete from area_risco where idt_base_risco IN (6);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 6, 'Áreas Quilombolas - INCRA', ST_Force2D(geom) from "shp/quilombolas" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 6;

analyze area_risco;

drop table "shp/quilombolas";
