delete from area_risco where idt_base_risco IN (4);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 4, 'Áreas Embargadas ICMBIO', ST_Force2D(geom) from "shp/embargos_icmbio" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 4;

analyze area_risco;

drop table "shp/embargos_icmbio";
