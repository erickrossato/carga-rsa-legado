#!/bin/sh

DB_NAME=gcredito_aws
DB_USER=usr_geo_credito_agricola
DB_SCHEMA=$DB_USER
DB_HOST=db-geocredito.ct9lbpnxzlpa.sa-east-1.rds.amazonaws.com
OUTPUT_FOLDER=out
BACKUP_FOLDER=bkp
#export PGPASSWORD=sccon@1234
export PGPASSWORD=4cd37d4fa6d54af29c113b5625434981

OUTPUT_FOLDER=${OUTPUT_FOLDER}/${DB_HOST}/${DB_NAME}
BACKUP_FOLDER=${BACKUP_FOLDER}/${DB_HOST}/${DB_NAME}

if [ ! -d "$OUTPUT_FOLDER" ]; then
	mkdir -p $OUTPUT_FOLDER
fi

if [ ! -d "$BACKUP_FOLDER" ]; then
	mkdir -p $BACKUP_FOLDER
fi

echo "Carregando terras indigenas"
psql -d $DB_NAME -f bkp/terras_indigenas.sql -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/terras_indigenas.sql.out

echo "Restaurando e fazendo intersects"
psql -d $DB_NAME -f restore_terras_indigenas.sql -h $DB_HOST -U $DB_USER -o ${OUTPUT_FOLDER}/restore_terras_indigenas.sql.out
