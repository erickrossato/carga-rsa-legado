#!/bin/bash

DB_USER=usr_geo_credito_agricola
DB_SCHEMA=$DB_USER
SQL_OUTPUT_FOLDER=sqls
OUTPUT_FOLDER=out
BACKUP_FOLDER=bkp
SHP_BASE_FOLDER=shp
TODAY=$( date +%Y-%m-%d_%HH%MM )
export PGPASSWORD=4cd37d4fa6d54af29c113b5625434981
#export PGPASSWORD=sccon@1234

while read line
do
	lineSplit=($line)
	dbName=${lineSplit[0]}
	dbHost=${lineSplit[1]}
	sqlOutputFolder=${SQL_OUTPUT_FOLDER}/${dbHost}/${dbName}/${TODAY}
	outputFolder=${OUTPUT_FOLDER}/${dbHost}/${dbName}/${TODAY}
	backupFolder=${BACKUP_FOLDER}/${dbHost}/${dbName}/${TODAY}

	if [ ! -d "$sqlOutputFolder" ]; then
		mkdir -p $sqlOutputFolder
	fi

	if [ ! -d "$outputFolder" ]; then
		mkdir -p $outputFolder
	fi

	if [ ! -d "$backupFolder" ]; then
		mkdir -p $backupFolder
	fi

	echo "Starting to process data for $dbName@$dbHost"
	echo "Backing up table municipio"
	pg_dump --host $dbHost --username $DB_USER -d $dbName -t 'municipio' > ${backupFolder}/municipio.sql

	echo "Starting to load shape file";

	for shapeFile in $( ls $SHP_BASE_FOLDER/*/*.shp  ); do

		folder=$( dirname $shapeFile )
		baseName=$( basename $shapeFile )

		echo "Generating SQL file for shapeFile $shapeFile"
		shp2pgsql -s 4674 -D $shapeFile $folder > ${sqlOutputFolder}/${baseName}.sql 2> /dev/null

		echo "Loading to database"
		psql -d $dbName -f ${sqlOutputFolder}/${baseName}.sql -h $dbHost -U $DB_USER -o ${outputFolder}/${baseName}.sql.out 2>&1

	done

	if [ -f "$1" ]; then

		echo "Running script $1 on database"
		psql -d $dbName -f $1 -h $dbHost -U $DB_USER -o ${outputFolder}/$1.out 2>&1

		echo "VACUUM ANALYZING municipio table"
		psql -d $dbName -h $dbHost -U $DB_USER -c 'VACUUM ANALYZE municipio;' 2>&1 1>/dev/null

	fi
done < $2
