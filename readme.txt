Executa script de carga de RSA na base de produção

 ./carrega-shape.sh load_ibama_audsat.sql servers_audsat_prod.txt


Carga ICMBIO

./carrega-shape.sh load_icmbio_audsat.sql servers_audsat_prod.txt
./carrega-shape.sh load_icmbio_sicredi.sql servers_sicredi_prod.txt
./carrega-shape-common-areas.sh load_icmbio_common_areas.sql 

UC Nao RESTRITIVA

./carrega-shape.sh load_uc_nr_audsat.sql servers_audsat_prod.txt
./carrega-shape.sh load_uc_nr_sicredi.sql servers_sicredi_prod.txt
./carrega-shape-common-areas.sh load_uc_nr_common_areas.sql

UC RESTRITIVA

./carrega-shape.sh load_uc_r_audsat.sql servers_audsat_prod.txt
./carrega-shape.sh load_uc_r_sicredi.sql servers_sicredi_prod.txt
./carrega-shape-common-areas.sh load_uc_r_common_areas.sql


Conferir:

select b.des_base_risco as "Base de Risco", b.dat_carregamento as "Última Atualização", count(a.*) as "Quantidade de Geometrias" from base_risco b
join area_risco a on b.idt_base_risco = a.idt_base_risco group by b.idt_base_risco;
