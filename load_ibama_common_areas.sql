delete from area_risco where idt_base_risco IN (3);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 3, 'Áreas Embargadas IBAMA', ST_Force2D(geom) from "shp/embargos_ibama" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 3;

analyze area_risco;

drop table "shp/embargos_ibama";
