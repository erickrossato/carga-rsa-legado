insert into area_risco(idt_area_risco, idt_base_risco, geo_area_risco, des_area_risco) select idt_area_risco, idt_base_risco, ST_Multi(geo_area_risco), des_area_risco from terras_indigenas;

insert into gleba_risco(idt_gleba, idt_area_risco, num_distancia)
select
	g.idt_gleba, ar.idt_area_risco, ST_Distance(g.geo_gleba, ar.geo_area_risco)
from 
	gleba g
	join area_risco ar on ST_DWithin(g.geo_gleba, ar.geo_area_risco, 0.005)
where
	ST_Distance(g.geo_gleba, ar.geo_area_risco) < 0.005
	AND ar.idt_base_risco = 1;

drop table terras_indigenas;