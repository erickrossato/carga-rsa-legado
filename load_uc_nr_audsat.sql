delete from matricula_risco mr where mr.idt_area_risco in (select idt_area_risco from area_risco where idt_base_risco IN (5));
delete from gleba_risco gr where gr.idt_area_risco in (select idt_area_risco from area_risco where idt_base_risco IN (5));
delete from area_risco where idt_base_risco IN (5);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 5, 'UCs - APAs e ARIEs', ST_Force2D(geom) from "shp/uc_nao_restritiva" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 5;

analyze area_risco;

insert into gleba_risco(idt_gleba, idt_area_risco, num_distancia)
select
	g.idt_gleba, ar.idt_area_risco, ST_Distance(g.geo_gleba, ar.geo_area_risco)
from 
	gleba g
	join area_risco ar on ST_DWithin(g.geo_gleba, ar.geo_area_risco, 0.005)
where
	ST_Distance(g.geo_gleba, ar.geo_area_risco) < 0.005
	AND ar.idt_base_risco IN (5);

insert into matricula_risco(idt_matricula, idt_area_risco, num_distancia)
select
	m.idt_matricula, ar.idt_area_risco, ST_Distance(m.geo_matricula, ar.geo_area_risco)
from 
	matricula m
	join area_risco ar on ST_DWithin(m.geo_matricula, ar.geo_area_risco, 0.005)
where
	ST_Distance(m.geo_matricula, ar.geo_area_risco) < 0.005
	AND ar.idt_base_risco IN (5);

drop table "shp/uc_nao_restritiva";
