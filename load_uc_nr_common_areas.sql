delete from area_risco where idt_base_risco IN (5);

insert into area_risco(idt_base_risco, des_area_risco, geo_area_risco)
select 5, 'UCs - APAs e ARIEs', ST_Force2D(geom) from "shp/uc_nao_restritiva" where geom is not null;

update base_risco set dat_carregamento=now()::timestamp where idt_base_risco = 5;

analyze area_risco;

drop table "shp/uc_nao_restritiva";
